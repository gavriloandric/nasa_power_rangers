$(document).ready(function () {

 	 $("#login-button").click(function(event){
 	 	var url_base = window.location.href;
		var email_hash = url_base.split('/').pop();
		var data = $('#forma').serialize();

		var pathArray = location.href.split( '/' );
        var protocol = pathArray[0];
        var host = pathArray[2];
        var url = protocol + '//' + host + '/send_beta_data/' + email_hash;

		$.post(url, data).success(function () {
            $('form').fadeOut(500);
	 	    $('.wrapper').addClass('form-success');
	 	    swal("Hvala", "Podaci su uspešno sačuvani", "success")
        }).error(function () {
            swal("Greška!", "Došlo je do greške, proverite da li ste uneli sva polja koja su obavezna.", "error");
        });
 		event.preventDefault();
    });
});
