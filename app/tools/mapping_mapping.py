import requests
import cStringIO
from PIL import Image
from math import sin, cos, sqrt, atan2, radians

"""
Place your Google Cloud API key in proper place
"""


def is_river(latitude, longitude):
    """
    Method for checking if place with given latitute and longitude is a river
    :param latitude: of wanted place
    :param longitude: wanted place
    :return: Success code
    """
    url = 'https://maps.googleapis.com/maps/api/staticmap?center={},%20{}&zoom=14&size=1x1&key=YOUR_KEY_HERE'.format(
        latitude, longitude)
    r = requests.get(url)
    if r.status_code != 200:
        print("There has been a mistake during request")
        return
    file = cStringIO.StringIO(r.content)
    img = Image.open(file)
    rgb_im = img.convert('RGB')
    r, g, b = rgb_im.getpixel((0, 0))
    print (r, g, b)
    if r == 163 and g == 203 and b == 255:
        return 1
    else:
        return 0


def distance(lat1, lon1, lat2, lon2):
    """
    Method for calculating distance beetween 2 points on Earth
    :param lat1: latitude of first place
    :param lon1: longitude of first place
    :param lat2: latitute of second place
    :param lon2: longitude of second place
    :return: calculated distance between 2 given places on Earth
    """
    R = 6373.0
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    print("Result:", distance)
    return distance


def get_rivers(lat, lon, omni=0, debug=0):
    """
    Method for getting rivers from area around given place
    :param lat: center latitude of selected area/crop
    :param lon: center longitude of selected area/crop
    :param omni: flag direction
    :param debug:  flag debug
    :return: list of nearby rivers, returns empty list if none found
    """
    start = {"lat": lat, "lon": lon}
    end = {"lat": lat, "lon": lon}
    list_of_rivers = []

    if debug:
        offest = 0.005
    else:
        offest = 0.0001

    sum_of_calls = 0
    i = 0
    while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
        i += 1
        print("In request {}".format(i))
        end['lon'] += offest
        print(end['lat'], end['lon'])
        dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
        if is_river(end['lat'], end['lon']):
            list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
            break
    sum_of_calls += i
    i = 0
    end = {"lat": lat, "lon": lon}
    while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
        i += 1
        print("In request {}".format(i))
        end['lon'] -= offest
        dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
        if is_river(end['lat'], end['lon']):
            list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
            break
    sum_of_calls += i
    i = 0
    end = {"lat": lat, "lon": lon}

    while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
        i += 1
        print("In request {}".format(i))
        end['lat'] -= offest
        dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
        if is_river(end['lat'], end['lon']):
            list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
            break
    sum_of_calls += i

    i = 0
    end = {"lat": lat, "lon": lon}
    while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
        i += 1
        print("In request {}".format(i))
        end['lat'] += offest
        dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
        if is_river(end['lat'], end['lon']):
            list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
            break
    sum_of_calls += i

    if omni:
        i = 0
        end = {"lat": lat, "lon": lon}
        while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
            i += 1
            print("In request {}".format(i))
            end['lat'] += offest
            end['lon'] += offest
            dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
            if is_river(end['lat'], end['lon']):
                list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
                break
        sum_of_calls += i

        i = 0
        end = {"lat": lat, "lon": lon}
        while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
            i += 1
            print("In request {}".format(i))
            end['lat'] -= offest
            end['lon'] -= offest
            dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
            if is_river(end['lat'], end['lon']):
                list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
                break
        sum_of_calls += i

        i = 0
        end = {"lat": lat, "lon": lon}
        while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
            i += 1
            print("In request {}".format(i))
            end['lat'] += offest
            end['lon'] -= offest
            dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
            if is_river(end['lat'], end['lon']):
                list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
                break
        sum_of_calls += i

        i = 0
        end = {"lat": lat, "lon": lon}
        while distance(start['lat'], start['lon'], end['lat'], end['lon']) < 1:
            i += 1
            print("In request {}".format(i))
            end['lat'] -= offest
            end['lon'] += offest
            dist = distance(start['lat'], start['lon'], end['lat'], end['lon'])
            if is_river(end['lat'], end['lon']):
                list_of_rivers.append({"lat": end['lat'], "lon": end['lon'], "distance": dist})
                break
        sum_of_calls += i

    print("Total number of call : {}".format(sum_of_calls))
    print("All of cooridantes for rivers")
    for x in list_of_rivers:
        print(x)
    return list_of_rivers


def sort(list_of_rivers):
    """
    Method for sorting list of rivers
    :param list_of_rivers:
    :return: sorted list of rivers
    """
    if not list_of_rivers:
        return []
    sortirano = sorted(list_of_rivers, key=lambda k: k['distance'])
    smaller = []
    if sortirano[0]:
        smaller.append(sortirano[0])
    if sortirano[1]:
        smaller.append(sortirano[1])
    return smaller
