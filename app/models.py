from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(64))
    email = db.Column(db.String(120), index=True, unique=True)
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    njiva_gore_lat = db.Column(db.Float)
    njiva_gore_lon = db.Column(db.Float)
    njiva_dole_lat = db.Column(db.Float)
    njiva_dole_lon = db.Column(db.Float)
    centar_lat = db.Column(db.Float)
    centar_lon = db.Column(db.Float)
    kultura = db.Column(db.String(128))
    rivers_lat1 = db.Column(db.Float)
    rivers_lon1 = db.Column(db.Float)
    rivers_dist1 = db.Column(db.Float)
    rivers_lat2 = db.Column(db.Float)
    rivers_lon2 = db.Column(db.Float)
    rivers_dist2 = db.Column(db.Float)

    def __repr__(self):
        return '<User %r>' % self.username

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the id to satisfy Flask-Login's requirements."""
        return self.id

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), index=True, unique=True)
    email_hash = db.Column(db.String(256), unique=True)
    created = db.Column(db.DateTime)
    verified_at = db.Column(db.DateTime)
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    crops = db.Column(db.String(32))
    location = db.Column(db.String(32))
    lot_width = db.Column(db.Float)
    lot_height = db.Column(db.Float)
    irr_count = db.Column(db.Integer)
    phone = db.Column(db.String(64))
    verified = db.Column(db.Boolean, default=False)