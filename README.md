In repo folder install virtuealenv

```
sudo apt-get install python-virtualenv
pip install virtuealenv
virtualenv nasa
```
Last command creates virtualenv nasa, so all packages will be installed for that interpreter

```
cd nasa
source bin/activate
cd ..
pip install -r requirements.txt
```

After requirements installation return to the root folder and create database

```
cd ..
python db_create.py
python db_migrate.py
python db_upgrade.py
```
After you create database everything is ready for running. To run the server do the following

```
python run.py
```

If everything went ok, web app is online on this address

```
http://localhost:5000
```