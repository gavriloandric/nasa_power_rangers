from app import app, db, lm, celery
from flask import render_template, request, redirect, flash, url_for, g
from flask_login import login_user, logout_user, current_user, login_required
from models import User, Contact
from tools.mapping_mapping import get_rivers, sort, distance
from flask import jsonify
from flask_mail import Message, Mail
from config import ADMINS
from datetime import datetime


@lm.user_loader
def load_user(id):
    return User.query.get(id)


@app.route('/')
def index_srb():
    return render_template('landing_rs.html')


@app.route('/eng')
def index_eng():
    return render_template('landing.html')


@app.route('/megazord')
@login_required
def dashboard():
    """
    Return main dashboard page
    :return: 
    """
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    Login page
    :return: 
    """
    if request.method == 'GET':
        return render_template('login.html')
    username = request.form['username']
    password = request.form['password']
    registered_user = User.query.filter_by(username=username, password=password).first()
    if registered_user is None:
        flash('Username or Password is invalid', 'error')
        return redirect(url_for('login'))
    login_user(registered_user)
    flash('Logged in successfully')
    return redirect(request.args.get('next') or url_for('dashboard'))


@app.route('/logout')
def logout():
    """
    Logout routine
    :return: 
    """
    logout_user()
    return redirect(url_for('login'))


@app.route('/megazord_ratar')
@login_required
def setapuj_njivu():
    """
    Returns page for lot init
    :return: 
    """
    return render_template('setapujnjivu.html')


@app.route('/actions/list_waters', methods=['GET'])
def list_waters():
    """
    API call for water locations
    :return: 
    """
    lat = request.args.get('lat')
    lon = request.args.get('lon')
    if (not lat) or (not lon):
        return "404: Parameters not provided"
    omni = request.args.get('omni')
    if not omni:
        omni = 1
    result = get_rivers(float(lat), float(lon), float(omni), 0)
    return jsonify(message=[{"all_waters": result}])


@app.route('/megazord_daje_njivu')
@login_required
def njiva_podaci():
    """
    API call returns data about users lot
    :return: 
    """
    ratar = User.query.filter(User.username == current_user.username).first()
    ratar.njiva_gore_lat = float(request.args.get('gore_desno_lat'))
    ratar.njiva_gore_lon = float(request.args.get('gore_desno_lng'))
    ratar.njiva_dole_lat = float(request.args.get('dole_levo_lat'))
    ratar.njiva_dole_lon = float(request.args.get('dole_levo_lng'))
    ratar.kultura = request.args.get('kultura')
    ratar.centar_lat = (ratar.njiva_gore_lat + ratar.njiva_dole_lat) / 2
    ratar.centar_lon = (ratar.njiva_gore_lon + ratar.njiva_dole_lon) / 2
    result = get_rivers(ratar.centar_lat, ratar.centar_lon, 0, 0)
    nearest_rivers = sort(result)
    if len(result) > 0:
        if nearest_rivers[0]:
            ratar.rivers_lat1 = nearest_rivers[0]['lat']
            ratar.rivers_lon1 = nearest_rivers[0]['lon']
            ratar.rivers_dist1 = nearest_rivers[0]['distance']
        if nearest_rivers[1]:
            ratar.rivers_lat2 = nearest_rivers[1]['lat']
            ratar.rivers_lon2 = nearest_rivers[1]['lon']
            ratar.rivers_dist2 = nearest_rivers[1]['distance']
    db.session.commit()
    return jsonify(nearest_rivers)


@app.route('/megazord_savetuje')
@login_required
def instructions():
    """
    Returns page with instructions about irrigation system
    :return: 
    """
    return render_template('uputstvo.html')


@app.route('/megazord_daruje_podatke_o_njivi')
@login_required
def megazord_daruje_podatke_o_njivi():
    """
    API call that returns lot coordinates
    :return: 
    """
    data = [{'gore_desno_lat': current_user.njiva_gore_lat,
             'gore_desno_lng': current_user.njiva_gore_lon,
             'dole_levo_lat': current_user.njiva_dole_lat,
             'dole_levo_lng': current_user.njiva_dole_lon}]
    print data
    return jsonify(message=data), 200


@app.route('/get_name')
@login_required
def get_name():
    """
    API call that returns user data, first and last name
    :return: 
    """
    data = [{'first_name': current_user.first_name,
             'last_name': current_user.last_name}]
    return jsonify(message=data), 200


@app.route('/get_water_coord')
@login_required
def water():
    """
    API call that returns water coordinates
    :return: 
    """
    lat1 = current_user.rivers_lat1
    lon1 = current_user.rivers_lon1
    lat2 = current_user.rivers_lat2
    lon2 = current_user.rivers_lon2
    data = [{'lat1': lat1,
             'lat2': lat2,
             'lon1': lon1,
             'lon2': lon2}]
    return jsonify(message=data), 200


@app.route('/create_user')
def create_user():
    """
    Create dummy user
    :return: 
    """
    user = User(username='megazord', password='power_rangers')
    db.session.add(user)
    db.session.commit()

    return jsonify({"user": 'megazord', "password": 'power_rangers'})


@app.route('/megazord_seljak')
def megazord_seljak():
    """
    API call that calculate parameters for irrigation system
    :return: 
    """
    width = distance(current_user.njiva_gore_lat, current_user.njiva_gore_lon, current_user.njiva_dole_lat,
                     current_user.njiva_gore_lon)
    hight = distance(current_user.njiva_gore_lat, current_user.njiva_gore_lon, current_user.njiva_gore_lat,
                     current_user.njiva_dole_lon)

    # hard coded crop type
    prored = 1
    izmedju_rupica = 1
    if current_user.kultura == 'psenica':
        prored = 100  # cm
        izmedju_rupica = 100  # cm
    elif current_user.kultura == 'kukuruz':
        prored = 75  # cm
        izmedju_rupica = 50  # cm
    broj_redova = width / prored * 100000
    ukupna_duzina_tankog = broj_redova * hight  # Total length for thin pipes
    dst = []
    try:
        dst.append(float(current_user.rivers_dist1))
    except:
        pass
    try:
        dst.append(float(current_user.rivers_dist2))
    except:
        pass
    if len(dst):
        avg_debelo = sum(dst) / len(dst)  # Average length for big pipes
    else:
        avg_debelo = 0.00
    data = {'prored': prored / 10,  # decimation
            'izmedju_rupica': izmedju_rupica,
            'broj_redova': int(broj_redova),
            'tanko': int(ukupna_duzina_tankog * 100),  # decimacija pa metara ostaje
            'debelo': int(avg_debelo * 1000)}
    return jsonify(data), 200


@celery.task
def send_async_email(subject, recipient, sender, body=None, html=None):
    app.config['MAIL_USERNAME'] = sender
    print app.config['MAIL_USERNAME']
    app.config['MAIL_DEFAULT_SENDER'] = sender
    mail = Mail(app)
    msg = Message(subject, recipients=[recipient])
    if body:
        msg.body = body
    if html:
        msg.html = html
    mail.send(msg)


@app.route('/send_mail', methods=['POST'])
def contact_us():
    message = request.form.get('text')
    email = request.form.get('email')
    subject = 'CONTACT'
    name = request.form.get('name')
    body = 'NAME >> ' + name + '\nEMAIL >> ' + email + '\nMESSAGE >> ' + message
    send_async_email.delay(subject=subject, recipient=ADMINS[0], sender=ADMINS[0], body=body)
    return 'ok'


@app.route('/beta_mail', methods=['POST'])
def beta_mail():
    message = 'BETA ACCESS'
    email = request.form.get('email')
    canonized_email = email.replace('@', '')
    canonized_email = canonized_email.replace('.', '')
    ratar = Contact.query.filter(Contact.email_hash == canonized_email).first()
    if ratar < 0:
        ratar = Contact()
        ratar.email = email
        ratar.email_hash = canonized_email
        ratar.created = datetime.today()
        db.session.add(ratar)
        try:
            db.session.add(ratar)
            db.session.commit()
        except:
            db.session.rollback()
    subject = 'BETA'
    body = 'NAME >> ' + '\nEMAIL >> ' + email + '\nMESSAGE >> ' + message
    send_async_email.delay(subject=subject, recipient=ADMINS[0], sender=ADMINS[0], body=body)

    beta = 'http://h2orticulture.space/beta_data/' + canonized_email
    send_async_email.delay(subject='Probni pristup H2Orticulture', recipient=email, sender=ADMINS[0],
                           html=render_template('email_beta_verification.html', beta=beta))
    return 'ok'


@app.route('/send_beta_data/<email_hash>', methods=['POST'])
def send_beta_form(email_hash):
    ratar = Contact.query.filter(Contact.email_hash == email_hash).first()
    if not ratar:
        return 'UPS!', 404
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    if ratar:
        ratar.first_name = first_name
        ratar.last_name = last_name
        ratar.crops = request.form.get('crop')
        ratar.location = request.form.get('location')
        ratar.lot_width = float(request.form.get('width'))
        ratar.lot_height = float(request.form.get('height'))
        ratar.irr_count = int(request.form.get('irrigation'))
        ratar.phone = request.form.get('phone')
        ratar.verified_at = datetime.today()
        ratar.verified = True
        try:
            db.session.commit()
        except:
            db.session.rollback()

        print 'ok'
    return 'ok'


@app.route('/beta_verification', methods=['POST'])
def beta_verification():

    return 'ok'


@app.route('/beta_form')
def beta_form_default():
    return render_template('beta_form.html')


@app.route('/beta_data/<email_hash>')
def beta_form(email_hash):
    return render_template('beta_form.html')


@app.route('/email')
def email():
    beta = 'beta_data/' + 'gavriloandricgmailcom'
    return render_template('email_beta_verification.html',  beta=beta)


@app.route('/send_beta_mail')
def beta_test():
    send_async_email.delay(subject='Beta test', recipient=ADMINS[0], sender=ADMINS[0], html=render_template('email_beta_verification.html', beta='www.google.rs'))
    return 'ok', 200


@app.route('/test_db')
def test():
    beta_users = Contact.query.filter().all()
    users = []
    for user in beta_users:
        users.append({'email': user.email})
    return jsonify(users), 200
